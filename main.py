import pygame as pg

from settings import *
from sprites import *


class Game:
    def __init__(self):
        pg.init()
        pg.mixer.init()
        self.screen = pg.display.set_mode((W_WIDTH, W_HEIGHT), pg.HWSURFACE | pg.DOUBLEBUF)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.allSprites = None
        self.playing = False

    def new(self):
        self.allSprites = pg.sprite.Group()
        self.player = Player()
        self.allSprites.add(self.player)
        self.run()

    def run(self):
        self.clock.tick(FPS)
        self.playing = True

        while self.playing:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    def update(self):
        self.allSprites.update()

    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False

    def draw(self):
        self.screen.fill(BLACK)
        self.allSprites.draw(self.screen)

        pg.display.flip()

    def show_start_screen(self):
        pass

    def show_go_screen(self):
        pass


game = Game()
game.show_start_screen()
while game.running:
    game.new()
    game.show_go_screen()

pg.quit()
